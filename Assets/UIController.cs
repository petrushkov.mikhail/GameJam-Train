﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIController : MonoBehaviour
{
    public Text Label;
    public CanvasGroup Fader;
    public ProgressBar Progress;
    public Image ChoChooseStarLogo;

    public GameObject DemoModeTip;

    private void Awake()
    {
        ChoChooseStarLogo.gameObject.SetActive(false);
    }

    public void ShowChooChooseLogo()
    {
        ChoChooseStarLogo.gameObject.SetActive(true);
        ChoChooseStarLogo.color = new Color(1, 1, 1, 0);
        ChoChooseStarLogo.DOColor(Color.white, 2f).Play();
    }

    public void HideChooChooseLogo()
    {
        ChoChooseStarLogo.gameObject.SetActive(false);
    }

    public void FadeOut()
    {
        Fader.gameObject.SetActive(true);
        Fader.alpha = 0;
        Fader.DOFade(1f, 1f);

        ChoChooseStarLogo.gameObject.SetActive(false);
    }

    public void FadeIn()
    {
        Fader.gameObject.SetActive(true);
        Fader.alpha = 1;

        if (string.IsNullOrEmpty(Label.text))
        {
            Fader.DOFade(0f, 2f).OnComplete(() => { Fader.gameObject.SetActive(false); });
        }
        else
        {
            Label.color = Color.white;
            Label.DOColor(Color.clear, 1f).SetDelay(1f);
            Fader.DOFade(0f, 2f).SetDelay(2f).OnComplete(() => { Fader.gameObject.SetActive(false); });
        }
    }

    public void SetFaderText(string text)
    {
        Label.text = text;
        Label.color = Color.clear;

        if (string.IsNullOrEmpty(text) == false)
        {
            Label.DOColor(Color.white, 2f).SetDelay(1f);
        }
        else
        {
        }
    }

    private void Update()
    {
        bool isDemoMode = Locator.Instance.GetStateManager().IsDemoMode;
        DemoModeTip.SetActive(isDemoMode);

        if (isDemoMode && Input.GetKeyDown(KeyCode.Space))
        {
            Locator.Instance.GetStateManager().StartGameSession(Locator.Instance.GetStateManager().MenuDecision);
        }
    }

#if UNITY_EDITOR
    private void OnGUI()
    {
        GUI.Label(new Rect(0, 0, 200, 100),
            "Logical = " + Locator.Instance.Player.GetParam(PlayerParameter.Logical) + "\n" + "Cats = " +
            Locator.Instance.Player.GetParam(PlayerParameter.KillCats) + "\n" + "CatLove = " +
            Locator.Instance.Player.GetParam(PlayerParameter.CatLove));
    }
#endif
}