﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Locator : MonoBehaviour
{
    public FinishUIController FinishUi;

    private static Locator _instance;
    public static Locator Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Locator>();
            }
            return _instance;
        }
    }

    public Camera MainCamera;
    private Transform _cameraTransform = null;
    public Transform CameraTransform
    {
        get {
            if(_cameraTransform == null)
            {
                _cameraTransform = MainCamera.transform;
            }
            return _cameraTransform;
        }
    }

    private PlayerProfile _playerProfile;
    public PlayerProfile Player
    {
        get {
            if(_playerProfile == null)
            {
                _playerProfile = new PlayerProfile();
            }
            return _playerProfile;
        }
    }

    private DecisionManager _decisionManager;
    public DecisionManager Decisions
    {
        get {
            if(_decisionManager == null)
            {
                _decisionManager = new DecisionManager();
            }
            return _decisionManager;
        }
    }

    private MapGenerator _mapGenerator;
    public MapGenerator GetMapGenerator()
    {
        if (_mapGenerator == null)
        {
            _mapGenerator = new MapGenerator();
        }
        return _mapGenerator;
    }

    private SoundManager _soundManager;
    public SoundManager GetSoundManager()
    {
        if (_soundManager == null)
        {
            _soundManager = new SoundManager();
        }
        return _soundManager;
    }

    private WorldController _worldController;
    public WorldController GetWorldController()
    {
        if (_worldController == null)
        {
            _worldController = GameObject.FindObjectOfType<WorldController>();
        }
        return _worldController;
    }

    private UIController _uiController;
    public UIController GetUIController()
    {
        if (_uiController == null)
        {
            _uiController = GameObject.FindObjectOfType<UIController>();
        }
        return _uiController;
    }

    [SerializeField] private PauseController _pauseController;
    public PauseController GetPauseController()
    {
        if (_pauseController== null)
        {
            _pauseController= GameObject.FindObjectOfType<PauseController>();
        }
        return _pauseController;
    }

    public FinishUIController GetFinishUIController()
    {
        return FinishUi;
    }

    private StateFactory _stateFactory;
    public StateFactory GetStateFactory()
    {
        if (_stateFactory == null)
        {
            _stateFactory = new StateFactory();
        }
        return _stateFactory;
    }

    private StateManager _stateManager;
    public StateManager GetStateManager()
    {
        if (_stateManager== null)
        {
            _stateManager= GameObject.FindObjectOfType<StateManager>();
        }
        return _stateManager;
    }

}
