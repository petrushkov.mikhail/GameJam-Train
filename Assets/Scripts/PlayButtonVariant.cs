﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButtonVariant : AbstractVariantController
{
    public override void Init(AbstractVariantController[] n)
    {
    }

    public override void OnChooseMe()
    {
    }

    public override void OnChooseNotMe()
    {
    }

    public override void OnBeginDriveOnMe()
    {
    }

    public override void OnDriveOnMe()
    {
    }

    public override void OnEndDriveOnMe()
    {
    }
}
