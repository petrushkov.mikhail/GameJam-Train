﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator
{
    private List<List<AbstractVariantController>> _arts = new List<List<AbstractVariantController>>();
    private bool _isInverted = false;

    public void Generate(Decision decision, bool inverted)
    {
        _isInverted = inverted;

        for (int i = 0; i < _arts.Count; i++)
        {
            if (_arts[i] != null)
            {
                for (int j = 0; j < _arts[i].Count; j++)
                {
                    GameObject.Destroy(_arts[i][j].gameObject);
                }
            }
        }
        _arts.Clear();

        if (decision != null && decision.VariantsArt != null)
            for (int i = 0; i < decision.VariantsArt.Length; i++)
            {
                int mapVariantPos = inverted ? (decision.VariantsArt.Length - i - 1) : i;
                List<AbstractVariantController> controller = null;
                if (decision.VariantsArt[i] != null)
                {
                    controller = new List<AbstractVariantController>();
                    for (int j = 0; j < decision.VariantsArt[i].Length; j++)
                    {
                        if(decision.VariantsArt[i][j] == null) continue;

                        GameObject go = GameObject.Instantiate(decision.VariantsArt[i][j].gameObject);
                        var variant = go.GetComponent<AbstractVariantController>();
                        variant.gameObject.transform.position = Locator.Instance.GetWorldController().VariantPos[mapVariantPos].GetChild(j).position;
                        controller.Add(variant);
                    }
                    for (int j = 0; j < controller.Count; j++)
                    {
                        controller[j].Init(controller.ToArray());
                    }
                }
                _arts.Add(controller);
            }
    }

    public void ChoosePath(int id)
    {
        Locator.Instance.GetWorldController().Train.OnChoose(id);
        
        id = (_isInverted) ? (1-id) : id;
        for (int i = 0; i < _arts.Count; i++)
        {
            if (_arts[i] != null)
            {
                for (int j = 0; j < _arts[i].Count; j++)
                {
                    if (i == id)
                    {
                        _arts[i][j].OnChooseMe();
                    }
                    else
                    {
                        _arts[i][j].OnChooseNotMe();
                    }
                }
            }
        }
    }

    public void OnBeginDriveOn(int id)
    {
        id = (_isInverted) ? (1-id) : id;
        if (_arts.Count > id && _arts[id] != null)
        {
            for (int j = 0; j < _arts[id].Count; j++)
            {
                _arts[id][j].OnBeginDriveOnMe();
            }
        }
    }
}
