﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecisionManager
{
    private int _currentStep;
    private List<Decision> _doneDecisions = new List<Decision>();
    private List<Decision> _blockedDecisions = new List<Decision>();
    public static readonly int[] Sequence = new int[] { 0, 0, 1, 2, 3, 1, 2, 1, 3, 3, 4 };
    // public static readonly int[] Sequence = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 4 };
    // public static readonly int[] Sequence = new int[] { 3, 4 };

    private Dictionary<int, List<Decision>> _decisionsByRank = new Dictionary<int, List<Decision>>();

    private Decision[] _decisions;

    public DecisionManager()
    {
        GetAllDecisions();
        for (int i = 0; i < _decisions.Length; i++)
        {
            _decisions[i].AddWeight();
        }
    }


    public Decision[] GetAllDecisions()
    {
        if (_decisions == null)
        {
            _decisions = DecisionList.Instance.List;
            for (int i = 0; i <= 4; i++)
            {
                _decisionsByRank.Add(i, new List<Decision>());
            }

            for (int i = 0; i < _decisions.Length; i++)
            {
                _decisionsByRank[_decisions[i].Rank].Add(_decisions[i]);
            }
        }

        return _decisions;
    }

    public Decision GetDecision(string name)
    {
        GetAllDecisions();
        for (int i = 0; i < _decisions.Length; i++)
        {
            if (_decisions[i].Id.Equals(name))
            {
                return _decisions[i];
            }
        }
        return null;
    }

    public int GurrentStep
    {
        get
        {
            return _currentStep;
        }
    }

    private List<Decision> GetDecicionsWithWeight(int rank)
    {
        // var res = "";
        List<Decision> decisionList = new List<Decision>();
        for (int i = 0; i < _decisionsByRank[rank].Count; i++)
        {
            for (int j = 0; j < _decisionsByRank[rank][i].GetWeight(); j++)
            {
                decisionList.Add(_decisionsByRank[rank][i]);
                // res += _decisionsByRank[rank][i].name + ":" + _decisionsByRank[rank][i].GetWeight() + ",";
            }
        }
        // Debug.Log(res);
        return decisionList;
    }

    public Decision GetNextDecision()
    {
        Decision decision = null;
        GetAllDecisions();

        if (_currentStep >= Sequence.Length)
        {
            return null;
        }

        int rank = Sequence[_currentStep];
        int errorCounter = 0;

        while (errorCounter < 100)
        {
            errorCounter++;

            if (rank < 4)
            {
                var decisionListWithWeight = GetDecicionsWithWeight(rank);
                if (decisionListWithWeight.Count == 0)
                {
                    for (int i = 0; i < _decisionsByRank[rank].Count; i++)
                    {
                        _decisionsByRank[rank][i].AddWeight();
                    }
                    decisionListWithWeight = GetDecicionsWithWeight(rank);
                }
                int randomDecision = Random.Range(0, decisionListWithWeight.Count);
                decision = decisionListWithWeight[randomDecision];
            }
            else
            {
                int logical = Locator.Instance.Player.GetParam(PlayerParameter.Logical);
                int cats = Locator.Instance.Player.GetParam(PlayerParameter.KillCats);

                // logical = 0..5
                 Debug.Log("Logical = " + logical);
                 Debug.Log("Cats = " + cats);

                var needEnding = "Cat-Man";
                if (logical <= 1)
                {
                    needEnding = "5vs5";
                }
                else if (logical <= 4)
                {
                    if (cats > 0)
                    {
                        needEnding = "Mom-Dad";
                    }
                    else
                    {
                        needEnding = "Cat-Man";
                    }
                }
                else
                {
                    needEnding = "You-Man";
                }
                Debug.Log("Ending: " + needEnding);

                for (int i = 0; i < _decisionsByRank[rank].Count; i++)
                {
                    if (_decisionsByRank[rank][i].Id.Equals(needEnding))
                    {
                        decision = _decisionsByRank[rank][i];
                        break;
                    }
                }
            }

            if (!_doneDecisions.Contains(decision) && !_blockedDecisions.Contains(decision))
            {
                break;
            }
        }

        if (errorCounter >= 100)
        {
            Debug.Log("Error to found decision rank " + rank);
            decision = _decisionsByRank[rank][Random.Range(0, _decisionsByRank[rank].Count)];
        }

        if (decision != null)
        {
            _doneDecisions.Add(decision);

            if (decision.BlocksDecisions != null)
            {
                _blockedDecisions.AddRange(decision.BlocksDecisions);
            }
            decision.Used();
        }

        _currentStep++;
        return decision;
    }

    public void ResetGame()
    {
        _currentStep = 0;
        _doneDecisions.Clear();
        _blockedDecisions.Clear();
    }
}
