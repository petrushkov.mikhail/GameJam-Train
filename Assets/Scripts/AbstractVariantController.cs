﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractVariantController : MonoBehaviour
{
    void Awake()
    {
        gameObject.tag = "Variant";
    }

    [ContextMenu ("Rotate")]
    void DoSomething ()
    {
        OnEnable();
    }

    void OnEnable()
    {
        transform.LookAt(transform.position + Locator.Instance.CameraTransform.rotation * Vector3.forward,
            Locator.Instance.CameraTransform.rotation * Vector3.up);
    }


    private SpriteRenderer _renderer;
    protected SpriteRenderer GetRenderer()
    {
        if (_renderer == null)
        {
            _renderer = GetComponent<SpriteRenderer>();
        }
		return _renderer;
    }

    public abstract void Init(AbstractVariantController[] n);

    public abstract void OnChooseMe();
    public abstract void OnChooseNotMe();
    public abstract void OnBeginDriveOnMe();

    public abstract void OnDriveOnMe();
    public abstract void OnEndDriveOnMe();
}
