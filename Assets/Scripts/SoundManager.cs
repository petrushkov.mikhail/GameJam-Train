﻿using UnityEngine;
using System.Collections.Generic;

public class SoundManager
{
    private bool _isInited = false;

    public Dictionary<string, AudioSource> sounds = new Dictionary<string, AudioSource>();
    private const int MUS_SND_LENGTH = 5;

    void Awake()
    {
        CheckSounds();
    }

    private void CheckSounds()
    {
        SetMusicLevel(GetMusicLevel());
        SetSoundLevel(GetSoundLevel());

        sounds = new Dictionary<string, AudioSource>();
        var asourses = Resources.FindObjectsOfTypeAll<AudioSource>();
        foreach (AudioSource a in asourses)
        {
            if (sounds.ContainsKey(a.gameObject.name) == false)
            {
                sounds.Add(a.gameObject.name, a);
                a.playOnAwake = false;
            }
        }
        _isInited = true;
    }

    public AudioSource LoadSound(string name)
    {
        if(_isInited == false)
        {
            CheckSounds();
        }
        if (sounds.ContainsKey(name))
        {
            var clip = sounds[name];
            return clip;
        }
        else
        {
            var source = Resources.Load("Sounds/" + name) as AudioClip;
            if (source != null)
            {
                var pGO = new GameObject();
                pGO.name = "Sound_" + name;
                var asource = pGO.AddComponent<AudioSource>();
                asource.clip = source;
                asource.playOnAwake = false;
                sounds.Add(name, asource);
                asource.volume = source.length >= MUS_SND_LENGTH ? this.GetMusicLevel() : this.GetSoundLevel();
                return asource;
            }
            else
            {
                Debug.Log("Sound not found: " + name);
            }

        }

        return null;
    }

    public void Play(string name, bool loop = false, bool restartIfPlaying = true)
    {
        if(string.IsNullOrEmpty(name))
        {
            return;
        }
        
        var clip = LoadSound(name);

        if (clip != null)
        {
            if (clip.isPlaying && restartIfPlaying == false)
            {
                return;
            }
            clip.loop = loop;
            clip.Play();
        }
    }

    public bool IsPlaying(string name)
    {
        if (sounds.ContainsKey(name))
        {
            var clip = sounds[name];
            if (clip.isPlaying)
                return true;
        }
        return false;
    }

    public void Stop(string name)
    {
        if (sounds.ContainsKey(name))
        {
            var clip = sounds[name];
            if (clip != null)
            {
                clip.Stop();
            }
        }
    }

    public void SetMusicLevel(float value)
    {
        if (sounds != null)
        {
            PlayerPrefs.SetFloat("musicLevel", value);
            foreach (var a in sounds)
            {
                if (a.Value != null)
                {
                    if (a.Value.clip.length >= MUS_SND_LENGTH)
                    {
                        a.Value.volume = value;
                    }
                }
            }
        }
    }

    public void SetSoundLevel(float value)
    {
        if (sounds != null)
        {
            PlayerPrefs.SetFloat("soundLevel", value);
            foreach (var a in sounds)
            {
                if (a.Value != null)
                {
                    if (a.Value.clip.length < MUS_SND_LENGTH)
                    {
                        a.Value.volume = value;
                    }
                }
            }
        }
    }

    public float GetMusicLevel()
    {
        return PlayerPrefs.GetFloat("musicLevel", 1);
    }

    public float GetSoundLevel()
    {
        return PlayerPrefs.GetFloat("soundLevel", 1);
    }

}
