﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Decision")]
public class Decision : ScriptableObject
{
    public string Id;
    // public GameObject Background;
    // public GameObject Rails;
    // public GameObject Forest;
    public DecisionArtArray[] VariantsArt;
    public int Rank;
    public Decision[] BlocksDecisions;
    public DecisionInfluence[] Influence;

    private int _sessionTimes = 0;

    public int GetWeight()
    {
        if (name.Equals("Cat-Dog"))
        {
            return 100;
        }
        else
        {
            return PlayerPrefs.GetInt("Decision_" + this.name, 0);
        }
    }

    public void AddWeight()
    {
        PlayerPrefs.SetInt("Decision_" + this.name, GetWeight() + 1);
    }

    public void Used()
    {
        PlayerPrefs.SetInt("Decision_" + this.name, 0);
        PlayerPrefs.SetInt("Decision_" + this.name + "_alltime", AchievedAllTimes() + 1);
    }

    public int AchievedAllTimes()
    {
        return _sessionTimes;
        // return PlayerPrefs.GetInt("Decision_" + this.name + "_alltime", 0);
    }
}

[System.Serializable]
public class DecisionInfluence
{
    public bool FirstTrack;
    public PlayerParameter Parameter;
    public int Value;
}

[System.Serializable]
public class DecisionArtArray
{
    public int Length
    {
        get
        {
            return VariantsArt.Length;
        }
    }
    public AbstractVariantController this[int key]
    {
        get
        {
            return VariantsArt[key];
        }
        set
        {
            VariantsArt[key] = value;
        }
    }
    public AbstractVariantController[] VariantsArt;
}