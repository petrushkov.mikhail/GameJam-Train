using UnityEngine;

[CreateAssetMenu(menuName = "DecisionList")]
public class DecisionList : ScriptableObject
{
    public Decision[] List;

    private static DecisionList _instance;
    public static DecisionList Instance
    {
        get {
            if(_instance == null)
            {
                _instance = Resources.Load<DecisionList>("DecisionList");
            }
            return _instance;
        }
    }
}