﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Billboard : MonoBehaviour {
    void OnEnable()
    {
        transform.LookAt(transform.position + Locator.Instance.CameraTransform.rotation * Vector3.forward,
            Locator.Instance.CameraTransform.rotation * Vector3.up);
    }

    [ContextMenu ("Rotate")]
    void DoSomething ()
    {
        OnEnable();
    }

}
