﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveState : AbstractState
{
    private float _timer = 0;
    private int _currentChoosedPath = 0;
    private bool _isMenuLevel = false;
    // private Decision _currentDecision;

    public override void Show(object[] args = null)
    {
        _currentChoosedPath = (int)args[0];
        _isMenuLevel = (bool)args[2];

        Locator.Instance.GetWorldController().GoPath(_currentChoosedPath);
        _timer = 5f;
        Locator.Instance.GetMapGenerator().OnBeginDriveOn(_currentChoosedPath);
        Locator.Instance.GetWorldController().Train.OnDriveBegin(_currentChoosedPath);
        Locator.Instance.GetWorldController().ChoosePath(-1);

        if (_isMenuLevel)
        {
            Locator.Instance.GetUIController().ShowChooChooseLogo();
        }
    }

    public override void Hide()
    {

    }

    public override IState GetNextState()
    {
        return Locator.Instance.GetStateFactory().GetState<FinalState>();
    }

    public override void Update()
    {
        if (_timer > 0)
        {
            _timer -= Time.deltaTime;
            if (_timer <= 0)
            {
                Decision nextDecision = Locator.Instance.Decisions.GetNextDecision();
                if (nextDecision == null)
                {
                    Locator.Instance.GetFinishUIController().Show();
                }
                else
                {
                    _manager.GoNextState(new object[] { nextDecision, _isMenuLevel });
                }
            }
        }
    }
}
