﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalState : AbstractState
{
    private object[] _args = null;
    private float _timer;



    public override void Show(object[] args = null)
    {
        _args = args;
        Decision decision = (Decision)_args[0];

        _timer = 1;
        var text = "";

        // Debug.Log(decision.name);

        if (decision.Rank == 4)
        {
            _timer = 3;

            switch (decision.AchievedAllTimes())
            {
                case 1:
                case 0:
                    text = "Grand Finale";
                    break;
                case 2:
                    text = "And yet we're here once again";
                    break;
                default:
                    text = "What are the choices that lead us there?";
                    break;
            }
            // Debug.Log("Final label: "+text);
        }
        else if(decision.Rank == 3 || decision.Rank == 2)
        {
            _timer = 2f;
        }
        Locator.Instance.GetUIController().SetFaderText(text);
        Locator.Instance.GetUIController().FadeOut();

    }

    private void GoNext()
    {
        if ((bool)_args[1] == true)
        {
            _manager.StartGameSession();
        }
        else
        {
            _manager.GoNextState(new object[] { _args[0] });
        }
    }

    public override void Hide()
    {

    }

    public override IState GetNextState()
    {
        return Locator.Instance.GetStateFactory().GetState<InitState>();
    }

    public override void Update()
    {
        if (_timer > 0)
        {
            _timer -= Time.deltaTime;
            if (_timer <= 0)
            {
                GoNext();
            }
        }
    }
}
