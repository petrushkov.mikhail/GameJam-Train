﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractState : IState {
	protected StateManager _manager;
	public void Init(StateManager manager)
	{
		_manager = manager;
	}

	public void Show()
	{
		Show(null);
	}

	public abstract void Show(object[] args);
	public abstract void Hide();
	public abstract IState GetNextState();
	
	public virtual void Update()
	{

	}
}
