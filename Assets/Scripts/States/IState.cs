﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IState {
	void Init(StateManager manager);
	void Show();
	void Show(object[] args);
	void Hide();
	IState GetNextState();
	void Update();
}
