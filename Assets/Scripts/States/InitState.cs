﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitState : AbstractState
{
    private Decision _currentDecision;
    private int _currentChoosedPath = 0;
    private float _lastMouseCheck = 0;
    private bool _isMenuLevel = false;
    private bool _inverted = false;
    private float _showTime = 0;

    public override void Show(object[] args = null)
    {
        if (args == null)
        {
            return;
        }
        else
        {
            _currentDecision = (Decision)args[0];
            if (args.Length > 1 && (bool)args[1] == true)
            {
                Locator.Instance.Decisions.ResetGame();
            }
            _isMenuLevel = false;
            if (args.Length > 2)
            {
                _isMenuLevel = true;
            }
        }

        var len = (_isMenuLevel) ? -1 : 5;

        _inverted = Random.Range(0, 2) == 0 ? false : true;
        if (_currentDecision.VariantsArt != null && _currentDecision.VariantsArt.Length > 1 && _currentDecision.VariantsArt[1].Length > 5)
        {
            _inverted = false;
        }

        Locator.Instance.GetMapGenerator().Generate(_currentDecision, _inverted);
        Locator.Instance.GetWorldController().StartTrain(() =>
        {
            if (!_isMenuLevel)
            {
                StartMoving();
            }
        }, len);

        // ChoosePath(0);

        _lastMouseCheck = Time.time;

        Locator.Instance.GetUIController().FadeIn();
        if (_isMenuLevel)
        {
            Locator.Instance.GetUIController().Progress.SetProgress(0, 0);
        }
        else
        {
            Locator.Instance.GetUIController().Progress.SetProgress(
                Locator.Instance.Decisions.GurrentStep + 1, DecisionManager.Sequence.Length + 1);
        }

        if (Locator.Instance.GetStateManager().IsDemoMode)
        {
            int decision = Random.Range(0, 2);
            ChoosePath(decision);
            Locator.Instance.GetWorldController().ForceMove();
        }
        
        Locator.Instance.GetUIController().HideChooChooseLogo();

        _showTime = Time.time;

    }

    private void StartMoving()
    {
        var choosedReal = _inverted ? (1 - _currentChoosedPath) : _currentChoosedPath;
        for (int j = 0; j < _currentDecision.Influence.Length; j++)
        {
            if (_currentDecision.Influence[j].FirstTrack == (choosedReal == 0))
            {
                Locator.Instance.Player.SetParameter(
                    _currentDecision.Influence[j].Parameter,
                    _currentDecision.Influence[j].Value);

                // Debug.Log("Add influence: " + _currentDecision.Influence[j].Parameter + "+=" + _currentDecision.Influence[j].Value);
            }
        }
        _manager.GoNextState(new object[] { _currentChoosedPath, _currentDecision, _isMenuLevel });

    }

    private void ChoosePath(int id)
    {
        _currentChoosedPath = id;
        Locator.Instance.GetWorldController().ChoosePath(_currentChoosedPath);
        Locator.Instance.GetMapGenerator().ChoosePath(_currentChoosedPath);
    }

    public override void Hide()
    {
    }

    public override IState GetNextState()
    {
        return Locator.Instance.GetStateFactory().GetState<MoveState>();
    }

    public override void Update()
    {
        if(Locator.Instance.GetStateManager().IsDemoMode) return;
        if(Time.time < _showTime + 1f) return;
        
        
        if (Time.time - _lastMouseCheck > 0.05f)
        {
            _lastMouseCheck = Time.time;

            var oldPath = _currentChoosedPath;
            var newPath = DetectPath();
            if (newPath >= 0)
            {
                _currentChoosedPath = newPath;
                PlayChangeSound(oldPath);
                ChoosePath(_currentChoosedPath);
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (_manager.Pause.gameObject.activeSelf ||
            (Input.mousePosition.x > Screen.width * 0.9f && Input.mousePosition.y > Screen.height * 0.9f))
            {
                // Debug.Log("Pause");
            }
            else
            {
                var oldPath = _currentChoosedPath;
                var newPath = DetectPath();
                if (newPath >= 0)
                {
                    _currentChoosedPath = newPath;
                    PlayChangeSound(oldPath);

                    if (!_isMenuLevel)
                    {
                        Locator.Instance.GetWorldController().ForceMove();
                        _lastMouseCheck = float.MaxValue;
                    }
                    else
                    {
                        StartMoving();
                    }
                }
            }
        }
    }

    private int DetectPath()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        var all = Physics.RaycastAll(ray, 500);
        for (int i = 0; i < all.Length; i++)
        {
            if (all[i].transform.gameObject.tag.Equals("Rail"))
            {
                int path = -1;
                int.TryParse(all[i].transform.gameObject.name, out path);
                return path;
            }
        }
        return -1;
    }

    private void PlayChangeSound(int prev)
    {
        if (prev != _currentChoosedPath)
        {
            Locator.Instance.GetSoundManager().Play("snd/change_way");
        }
    }
}
