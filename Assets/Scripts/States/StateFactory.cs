﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateFactory
{
    private List<IState> _states = new List<IState>();

    public IState GetState<T>()
    {
		return GetState(typeof(T));
    }

    public IState GetState(System.Type type)
    {
        for (int i = 0; i < _states.Count; i++)
        {
            if (_states[i].GetType().Equals(type))
            {
                return _states[i];
            }
        }

        IState result = (IState)System.Activator.CreateInstance(type);
        _states.Add(result);
        return result;
    }
}
