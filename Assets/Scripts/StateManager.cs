﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{
    public Decision MenuDecision;
    public Decision DefaultDecision;
    private IState _currentState = null;
    public FinishUIController FinishUi;
    public PauseController Pause;

    public float TimeToGoToDemo = 45f;
    
    public bool IsDemoMode { get; private set; }

    void Start()
    {
        IsDemoMode = false;
        StartGameSession(MenuDecision);
    }

    public void StartGameSession(Decision first = null, bool resetDemo = true)
    {
        Locator.Instance.GetSoundManager().Play("m2", true, false);
        _currentState = null;
        FinishUi.gameObject.SetActive(false);

        Locator.Instance.Player.ResetGame();
        Locator.Instance.GetUIController().SetFaderText("");
        
        if(resetDemo) IsDemoMode = false;
        
        

        if (first == null)
        {
            GoNextState(new object[] { DefaultDecision, true });
        }
        else
        {
            GoNextState(new object[] { first, true, true });

            if(!IsDemoMode) StartCoroutine(StartDemoMode());
        }
    }

    public void GoNextState(object[] args = null)
    {
        if (_currentState != null)
        {
            _currentState.Hide();
            _currentState = _currentState.GetNextState();
        }
        else
        {
            _currentState = Locator.Instance.GetStateFactory().GetState<InitState>();
        }
        _currentState.Init(this);
        _currentState.Show(args);

        StopAllCoroutines();
    }

    private IEnumerator StartDemoMode()
    {
        yield return new WaitForSeconds(TimeToGoToDemo);
        IsDemoMode = true;
        StartGameSession(null, false);
    }

    void Update()
    {
        if (_currentState != null)
        {
            _currentState.Update();
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Pause.Show();
        }
    }
}