﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatVariantController : VariantController {
	public string CatSound = "snd/cat";
	public string DogSound = "snd/dog";

	public bool IsCatDefault = true;
	public Sprite CatSprite, DogSprite;
	

	void Awake()
	{
		var catLove = Locator.Instance.Player.GetParam(PlayerParameter.CatLove);
		if(catLove > 0)
		{
			HappyMan = CatSprite;
			SadMan = CatSprite;
			GetRenderer().sprite = CatSprite;
			SingleScream = CatSound;
			MultipleScream = CatSound;
		} else if (catLove < 0)
		{
			HappyMan = DogSprite;
			SadMan = DogSprite;
			GetRenderer().sprite = DogSprite;
			SingleScream = DogSound;
			MultipleScream = DogSound;
		}
		else
		{
			if (IsCatDefault)
			{
				HappyMan = CatSprite;
				SadMan = CatSprite;
				GetRenderer().sprite = CatSprite;
				SingleScream = CatSound;
				MultipleScream = CatSound;
			}
			else
			{
				HappyMan = DogSprite;
				SadMan = DogSprite;
				GetRenderer().sprite = DogSprite;
				SingleScream = DogSound;
				MultipleScream = DogSound;
			}
		}
	}

}
