﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum PlayerParameter
{
    Logical, CatLove, KillBabies, KillCats, KillPeople
}

public class PlayerProfile {
	private Dictionary<PlayerParameter, int> _params = new Dictionary<PlayerParameter, int>();

	public void SetParameter(PlayerParameter param, int value)
	{
		if(_params.ContainsKey(param) == false)
		{
			_params.Add(param, 0);
		}
		_params[param] += value;
	}

	public int GetParam(PlayerParameter param)
	{
		if(_params.ContainsKey(param))
		{
			return _params[param];
		}

		return 0;
	}

	public void ResetGame()
	{
		_params.Clear();
	}
}
