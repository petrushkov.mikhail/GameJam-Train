﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TrainController : MonoBehaviour
{
    public ParticleSystem[] SmokeWheels;
    public Transform[] Wheels;
   

    void Awake()
    {
        transform.DOScale(new Vector3(3, 1.8f, 1.5f), 0.33f).SetLoops(-1, LoopType.Yoyo);
    }

    public void OnChoose(int id)
    {
        for (int i = 0; i < SmokeWheels.Length; i++)
        {
            SmokeWheels[i].gameObject.SetActive(false);
        }
    }

    public void OnDriveBegin(int id)
    {
        // Debug.Log(id);
    }

    public void RotateWheels(float value)
    {
        for (int i = 0; i < Wheels.Length; i++)
        {
            Wheels[i].Rotate(0,0,value);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Variant"))
        {
            other.gameObject.GetComponent<AbstractVariantController>().OnDriveOnMe();

            for (int i = 0; i < SmokeWheels.Length; i++)
            {
                SmokeWheels[i].gameObject.SetActive(true);
                SmokeWheels[i].Clear();
                SmokeWheels[i].Play();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals("Variant"))
        {
            other.gameObject.GetComponent<AbstractVariantController>().OnEndDriveOnMe();
        }
    }
}
