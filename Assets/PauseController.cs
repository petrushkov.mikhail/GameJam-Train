﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseController : MonoBehaviour {
	public StateManager Manager;

	public void Show()
	{
		gameObject.SetActive(true);
		Time.timeScale = 0;
	}

	public void ExitGame()
	{
		Application.Quit();
	}

	public void Restart()
	{
		Time.timeScale = 1;
		gameObject.SetActive(false);
		Manager.StartGameSession(Manager.MenuDecision);
	}

	public void Resume()
	{
		gameObject.SetActive(false);
		Time.timeScale = 1;
	}
}
