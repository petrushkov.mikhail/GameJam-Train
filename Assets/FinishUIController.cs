﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FinishUIController : MonoBehaviour
{
    public float DelayToAutoRestart = 30f;
    public StateManager Manager;

    public CanvasGroup RepeatButton;
    public CanvasGroup[] Params;
    public Text FinalText;
    public Image Fader;

    public GameObject FinalTextState;
    public GameObject ParamsState;

    private int _current = 0;

    public void Show()
    {
        Locator.Instance.GetSoundManager().Stop("m2");

        gameObject.SetActive(true);

        Fader.color = Color.clear;
        Fader.DOColor(Color.black, 1f);

        ParamsState.SetActive(true);
        FinalTextState.SetActive(false);

        for (int i = 0; i < Params.Length; i++)
        {
            Params[i].gameObject.SetActive(false);
        }

        _current = 0;
        
        StopAllCoroutines();
        StartCoroutine(ShowFinalScreen());

        StartCoroutine(GoToStartMenu());
    }

    private IEnumerator ShowFinalScreen()
    {
        yield return new WaitForSeconds(2f);
        Locator.Instance.GetSoundManager().Play("dop/gong");

        var paramsOrder = new PlayerParameter[] {PlayerParameter.KillPeople, PlayerParameter.KillBabies,
        PlayerParameter.KillCats};

        if (Locator.Instance.Player.GetParam(PlayerParameter.CatLove) > 0)
        {
            Params[2].transform.GetChild(0).GetComponent<Text>().text = "Cats Killed";
        }
        else
        {
            Params[2].transform.GetChild(0).GetComponent<Text>().text = "Dogs Killed";
        }

        while (true)
        {
            if (_current < Params.Length)
            {
                Params[_current].gameObject.SetActive(true);

                var val = Locator.Instance.Player.GetParam(paramsOrder[_current]);
                Params[_current].transform.GetChild(1).GetComponent<Text>().text = val.ToString();

                Params[_current].alpha = 0;
                Params[_current].DOFade(1f, 1f);
            }
            else
            {
                ShowFinalText();
                break;
            }

            _current++;
            yield return new WaitForSeconds(2f);
        }
    }

    private void ShowFinalText()
    {
        ParamsState.SetActive(false);
        FinalTextState.SetActive(true);
        RepeatButton.gameObject.SetActive(false);
        FinalText.color = Color.clear;
        FinalText.DOColor(Color.white, 1f);
        StartCoroutine(ShowRepeat());
    }

    private IEnumerator ShowRepeat()
    {
        yield return new WaitForSeconds(3f);

        RepeatButton.gameObject.SetActive(true);
        RepeatButton.alpha = 0;
        RepeatButton.DOFade(1f, 1f);
    }

    private IEnumerator GoToStartMenu()
    {
        yield return new WaitForSeconds(DelayToAutoRestart);
        Manager.StartGameSession(Manager.MenuDecision);
    }

    public void PlayAgain()
    {
        Manager.StartGameSession();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
