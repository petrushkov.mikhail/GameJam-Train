﻿#if UNITY_EDITOR

using UnityEditor;

public class EditorUtils
{
    
    [MenuItem("Choo-choose/Reset")]
    public static void Reset()
    {
        UnityEngine.PlayerPrefs.DeleteAll();
    }
}

#endif