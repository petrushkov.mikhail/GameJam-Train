﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class VariantController : AbstractVariantController
{
    public string SingleScream = "man scream";
    public string MultipleScream = "scream";
    public string BubblePop = "bubble";

    public Sprite HappyMan;
    public Sprite SadMan;

    public ParticleSystem Blood;

    private bool _isTriggered = false;
    private AbstractVariantController[] _neig;
    private VariantController _this;

    public GameObject[] ActiveElements;

    public override void Init(AbstractVariantController[] n)
    {
        Blood.gameObject.SetActive(false);
        _isTriggered = false;
        _neig = n;
        _this = this;
    }

    public override void OnChooseMe()
    {
        if (GetRenderer() != null && SadMan != null)
        {
            GetRenderer().sprite = SadMan;
        }
    }

    public override void OnChooseNotMe()
    {
        if (GetRenderer() != null && HappyMan != null)
        {
            GetRenderer().sprite = HappyMan;
        }
    }

    public override void OnBeginDriveOnMe()
    {
        if (!string.IsNullOrEmpty(MultipleScream) && _neig.Count((v) => { return v.GetType().Equals(_this.GetType()); }) > 1)
        {
            Locator.Instance.GetSoundManager().Play(MultipleScream, false, false);
        }
        else if (!string.IsNullOrEmpty(SingleScream))
        {
            Locator.Instance.GetSoundManager().Play(SingleScream);
        }
    }

    public override void OnDriveOnMe()
    {
        if (_isTriggered == false)
        {
            _isTriggered = true;
            if (!string.IsNullOrEmpty(BubblePop))
            {
                Locator.Instance.GetSoundManager().Play(BubblePop, false, true);
            }
            Blood.gameObject.SetActive(true);
            Blood.Clear();
            Blood.Play();
            
            if (GetRenderer() != null)
            {
                GetRenderer().enabled = false;
            }
        }
    }

    public override void OnEndDriveOnMe()
    {
        if (ActiveElements != null)
        {
            for (int i = 0; i < ActiveElements.Length; i++)
            {
                ActiveElements[i].SetActive(false);
            }
        }
    }
}
