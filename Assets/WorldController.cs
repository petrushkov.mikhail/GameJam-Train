﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldController : MonoBehaviour
{
    public Transform SpawnPoint, StartSlowPoint, StayPoint;
    public Transform[] Path;
    public TrainController Train;
    private Rigidbody _trainRb;
    public Transform Choosers;
    public Transform[] VariantPos;

    private List<Transform> _currentPath = new List<Transform>();
    private Vector3 _step, _target;
    private Quaternion _targetRotation;

    private bool _isStarted = false;

    private System.Action _onComplete = null;
    private Vector3 _slowKoef = Vector3.zero;
    private bool _slower = false;

    private GameObject _decorations;
    public GameObject[] BackgroundPrefabs;

    public void StartTrain(System.Action onComplete = null, float time = 5)
    {
        _onComplete = onComplete;

        _trainRb = Train.GetComponent<Rigidbody>();
        _trainRb.transform.position = SpawnPoint.position;
        _target = Train.transform.position;
        _isStarted = true;

        _currentPath.Clear();

        if (time > 0)
        {
            _slowKoef = (StayPoint.position - StartSlowPoint.position) / time;
            _slower = true;
        }
        else
        {
            _slower = false;
        }
        _currentPath.Add(StartSlowPoint);
        _currentPath.Add(StayPoint);
        ChoosePath(-1);

        if(_decorations != null)
        {
            DestroyImmediate(_decorations);
        }
        var decor = BackgroundPrefabs[Random.Range(0, BackgroundPrefabs.Length)];
        _decorations = Instantiate(decor);
        _decorations.transform.SetParent(transform, false);
    }

    public void GoPath(int id)
    {
        _slowKoef = Vector3.zero;
        _slower = false;
        // Train.transform.position = StayPoint.position;
        _target = Train.transform.position;

        _currentPath.Clear();
        foreach (Transform t in Path[id])
        {
            _currentPath.Add(t);
        }
        _isStarted = true;
    }

    public void ChoosePath(int id)
    {
        foreach (Transform t in Choosers)
        {
            t.GetComponent<SpriteRenderer>().enabled = false;
        }
        if (id >= 0)
            Choosers.GetChild(id).GetComponent<SpriteRenderer>().enabled = true;
    }

    public void ForceMove()
    {
        _slower = false;
    }

    void FixedUpdate()
    {
        if (_isStarted)
        {
            // Debug.Log(_target);
            if (_currentPath.Count > 0)
            {
                if (_currentPath.Count == 1 && _slower)
                {
                    _target += _slowKoef * Time.deltaTime;
                }
                else
                {
                    _step = (_currentPath[0].position - _target).normalized;
                    _target += _step * Time.deltaTime * 5;
                }
                _targetRotation = _currentPath[0].rotation;

                if ((_currentPath[0].position - _target).sqrMagnitude < 0.01f)
                {
                    _currentPath.RemoveAt(0);
                }
            }
            else
            {
                _isStarted = false;
                if (_onComplete != null)
                {
                    _onComplete();
                    _onComplete = null;
                }
            }
        }

        // Debug.Log(Vector3.Lerp(Train.transform.position, _target, 0.1f));
        var prPosition = Train.transform.position;
        Train.transform.position = (Vector3.Lerp(Train.transform.position, _target, 0.1f));

        var angle = (Train.transform.position - prPosition).magnitude * 50;
        Train.RotateWheels(angle);
        
        Train.transform.rotation = Quaternion.Lerp(Train.transform.rotation, _targetRotation, 0.1f);
        // var v0 = Train.transform.rotation.eulerAngles.y;
        // var v = Train.transform.rotation.eulerAngles;
        // Train.transform.rotation = Quaternion.Euler(0, v.y, v.z);
        
        // _trainRb.MovePosition(Vector3.Lerp(Train.transform.position, _target, 0.1f));
        // _trainRb.MoveRotation(Quaternion.Lerp(Train.transform.rotation, _targetRotation, 0.1f));
    }
}
