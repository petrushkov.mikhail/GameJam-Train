﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    // public RectTransform Train, ThisRT;
    // private Vector2 _targetValue;

    public Text Label;

    // void Awake()
    // {
    // 	_targetValue = Train.anchoredPosition;
    // }

    public void SetProgress(int val, int all)
    {
        if (all == 0)
        {
            Label.text = "";
        }
        else
        {
            Label.text = string.Format("{0}<size=48>/{1}</size>", val, all);
        }
        // float w = ThisRT.sizeDelta.x;
        // _targetValue = new Vector2(w * val, 0);
        // if(Train.anchoredPosition.x > _targetValue.x)
        // {
        // 	Train.anchoredPosition = _targetValue;
        // }
    }

    // void Update()
    // {
    // 	Train.anchoredPosition = Vector2.Lerp(Train.anchoredPosition, _targetValue, 0.1f);
    // }
}
